#!/bin/bash

# variables
gcpArea="asia.gcr.io"
projectId="vivid-memento-379101"
gcpRegion="us-central1"
gkeClusterName="autopilot-test"
dockerImage="api-game-logic"

# script
docker login -u _json_key --password-stdin https://asia.gcr.io < key.json
docker build -t ${dockerImage} .
docker tag ${dockerImage} "${gcpArea}/${projectId}/${dockerImage}"
docker push "${gcpArea}/${projectId}/${dockerImage}:latest"

export USE_GKE_GCLOUD_AUTH_PLUGIN=True
gcloud container clusters get-credentials ${gkeClusterName} --region ${gcpRegion} --project ${projectId}
kubectl delete deployment ${dockerImage}
kubectl apply -f k8s-deployment.yaml
kubectl apply -f k8s-service.yaml
kubectl apply -f k8s-ingress.yaml