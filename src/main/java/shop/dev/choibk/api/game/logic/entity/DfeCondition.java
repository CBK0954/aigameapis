package shop.dev.choibk.api.game.logic.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.game.logic.enums.DfeKind;
import shop.dev.choibk.api.game.logic.model.DfeConditionCreateRequest;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DfeCondition {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "df-entity 이름")
    @Column(nullable = false, length = 50)
    private String dfeName;

    @ApiModelProperty(notes = "df-entity 타입")
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private DfeKind dfeKind;

    @ApiModelProperty(notes = "영향 여부")
    @Column(nullable = false)
    private Boolean isImpact;

    private DfeCondition(Builder builder) {
        this.dfeName = builder.dfeName;
        this.dfeKind = builder.dfeKind;
        this.isImpact = builder.isImpact;
    }

    public static class Builder implements CommonModelBuilder<DfeCondition> {
        private final String dfeName;
        private final DfeKind dfeKind;
        private final Boolean isImpact;

        public Builder(DfeConditionCreateRequest createRequest) {
            this.dfeName = createRequest.getDfeName();
            this.dfeKind = createRequest.getDfeCondition();
            this.isImpact = createRequest.getIsImpact();
        }

        @Override
        public DfeCondition build() {
            return new DfeCondition(this);
        }
    }
}