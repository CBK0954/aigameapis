package shop.dev.choibk.api.game.logic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DfeKind {
    POSITIVE("긍정", 10),
    NEGATIVE("부정", -10),
    NEUTRAL("중립", 0);

    private final String name;
    private final Integer weighted; // 곱셈 연산
}
