package shop.dev.choibk.api.game.logic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGameLogicApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiGameLogicApplication.class, args);
    }
}