package shop.dev.choibk.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.choibk.api.game.logic.model.GameRequest;
import shop.dev.choibk.api.game.logic.model.GameResponse;
import shop.dev.choibk.api.game.logic.service.GameService;
import shop.dev.choibk.common.response.model.SingleResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "대화")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/game")
public class GameController {
    private final GameService gameService;

    @ApiOperation(value = "개복치와 대화하기")
    @PostMapping("/send")
    public SingleResult<GameResponse> sendMessage(@RequestBody @Valid GameRequest gameRequest) {
     return ResponseService.getSingleResult(gameService.doCalculation(gameRequest));
    }
}