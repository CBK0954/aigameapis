package shop.dev.choibk.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.game.logic.entity.DfeCondition;
import shop.dev.choibk.api.game.logic.model.DialogFlowResponse;
import shop.dev.choibk.api.game.logic.model.GameRequest;
import shop.dev.choibk.api.game.logic.model.GameResponse;
import shop.dev.choibk.api.game.logic.repository.DfeConditionRepository;
import shop.dev.choibk.api.game.logic.repository.DieConditionRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GameService {
    private final DieConditionRepository dieConditionRepository;
    private final DfeConditionRepository dfeConditionRepository;
    private final DialogFlowService dialogFlowService;

    public GameResponse doCalculation(GameRequest gameRequest) {
        DialogFlowResponse dialogFlowResponse = dialogFlowService.detectIntentTexts(gameRequest.getMessage());

        // --------- 즉사여부 확인 ----------
        // DieCondition 에서 위에서 가져온 displayName 에 해당하는 값이 있는지 확인함 -> count 로 확인
        long intentDuplicateCount = dieConditionRepository.countByIntentName(dialogFlowResponse.getIntentName());

        // 만약 데이터가 있으면 즉사처리
        if (intentDuplicateCount > 0) {
            return new GameResponse.Builder(dialogFlowResponse.getQueryText(), dialogFlowResponse.getFulfillmentText(), true, 0).build();
        } else { // 데이터가 없으면 점수 계산하는 2단계로 넘어감
            return new GameResponse.Builder(dialogFlowResponse.getQueryText(), dialogFlowResponse.getFulfillmentText(), false, calculateChangeVitalityFigures(dialogFlowResponse.getEntitys())).build();
        }
        // ---- 즉사여부 확인 끝 -----
    }

    private int calculateChangeVitalityFigures(List<String> paramKeys) {
        List<DfeCondition> dfeConditions = new LinkedList<>();

        // 내가 가진 키들의 상세 데이터를 dfeConditions 에 모
        for (String key : paramKeys) {
            Optional<DfeCondition> tempResult = dfeConditionRepository.findByDfeName(key);
            tempResult.ifPresent(dfeConditions::add);
        }

        int weightValue = 0; // 최종 가중치
        int weightCount = 0; // 수치 영향 여부 총 갯수
        for (DfeCondition dfeCondition : dfeConditions) {
            weightValue += dfeCondition.getDfeKind().getWeighted();
            if (dfeCondition.getIsImpact()) {
                weightCount += 1;
            }
        }
        return weightValue * weightCount;
    }
}