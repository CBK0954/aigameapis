package shop.dev.choibk.api.game.logic.service;

import shop.dev.choibk.api.game.logic.model.DialogFlowResponse;
import com.google.api.gax.rpc.ApiException;

import com.google.cloud.dialogflow.v2.DetectIntentResponse;
import com.google.cloud.dialogflow.v2.QueryInput;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.cloud.dialogflow.v2.SessionsClient;
import com.google.cloud.dialogflow.v2.TextInput;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class DialogFlowService {
    public DialogFlowResponse detectIntentTexts(String callMsg) throws ApiException {
        String projectId = "vivid-memento-379101"; // ID 확인 필수

        String randomSessionId = UUID.randomUUID().toString();

        String langCode = "ko-KR";

        // Instantiates a client
        try (SessionsClient sessionsClient = SessionsClient.create()) {
            // Set the session name using the sessionId (UUID) and projectID (my-project-id)
            SessionName session = SessionName.of(projectId, randomSessionId);

            TextInput.Builder textInput = TextInput.newBuilder().setText(callMsg).setLanguageCode(langCode);

            // Build the query with the TextInput
            QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();

            // Performs the detect intent request
            DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);

            QueryResult result = response.getQueryResult();

            DialogFlowResponse resultResponse = new DialogFlowResponse();
            resultResponse.setQueryText(result.getQueryText());
            resultResponse.setFulfillmentText(result.getFulfillmentText());
            resultResponse.setIntentName(result.getIntent().getDisplayName());

            List<String> dfEntitys = new LinkedList<>();
            Struct temp1 = result.getParameters();
            Map<String, Value> temp2 = temp1.getFieldsMap();
            temp2.forEach((key, value) -> {
                dfEntitys.add(key);
            });
            resultResponse.setEntitys(dfEntitys);

            return resultResponse;

        } catch (Exception e) {
            return null;
        }
    }
}