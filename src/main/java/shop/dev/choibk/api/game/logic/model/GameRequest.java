package shop.dev.choibk.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GameRequest {
    @ApiModelProperty(notes = "개복치에게 요청할 말")
    @NotNull
    @Length(min = 5, max = 100)
    private String message;
}
