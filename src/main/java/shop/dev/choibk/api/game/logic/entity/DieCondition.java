package shop.dev.choibk.api.game.logic.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.game.logic.model.DieConditionCreateRequest;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DieCondition {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "intent 명")
    @Column(nullable = false, length = 150)
    private String intentName;

    private DieCondition(Builder builder) {
        this.intentName = builder.intentName;
    }

    public static class Builder implements CommonModelBuilder<DieCondition> {
        private final String intentName;

        public Builder(DieConditionCreateRequest conditionCreateRequest) {
            this.intentName = conditionCreateRequest.getIntentName();
        }

        @Override
        public DieCondition build() {
            return new DieCondition(this);
        }
    }
}

