package shop.dev.choibk.api.game.logic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.game.logic.entity.DieCondition;

public interface DieConditionRepository extends JpaRepository<DieCondition, Long> {
    long countByIntentName(String intentName);
}

