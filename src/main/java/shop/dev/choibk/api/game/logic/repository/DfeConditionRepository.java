package shop.dev.choibk.api.game.logic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.game.logic.entity.DfeCondition;

import java.util.Optional;

public interface DfeConditionRepository extends JpaRepository<DfeCondition, Long> {
    Optional<DfeCondition> findByDfeName(String dfeName);
}

