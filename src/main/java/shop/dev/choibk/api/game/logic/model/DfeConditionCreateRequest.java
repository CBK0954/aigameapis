package shop.dev.choibk.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.dev.choibk.api.game.logic.enums.DfeKind;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DfeConditionCreateRequest {
    @ApiModelProperty(notes = "df-entity 이름")
    @NotNull
    @Length(min = 2, max = 50)
    private String dfeName;

    @ApiModelProperty(notes = "df-entity 타입")
    @NotNull
    @Enumerated(EnumType.STRING)
    private DfeKind dfeCondition;

    @ApiModelProperty(notes = "영향 여부")
    @NotNull
    private Boolean isImpact;
}
