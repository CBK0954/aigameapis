package shop.dev.choibk.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DieConditionCreateRequest {
    @ApiModelProperty(notes = "intent 명", required = true)
    @NotNull
    @Length(min = 2, max = 150)
    private String intentName;
}
