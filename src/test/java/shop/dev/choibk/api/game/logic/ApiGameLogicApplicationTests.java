package shop.dev.choibk.api.game.logic;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import shop.dev.choibk.api.game.logic.model.DialogFlowResponse;
import shop.dev.choibk.api.game.logic.service.DialogFlowService;

import java.io.IOException;

@SpringBootTest
public class ApiGameLogicApplicationTests {
    @Autowired
    DialogFlowService dialogFlowService;

    @Test
    void contextLoads() throws IOException {
        String callMsg = "안녕";
        DialogFlowResponse result = dialogFlowService.detectIntentTexts(callMsg);

        int a = 1;
    }
}